package be.kdg.operational;

import be.kdg.operational.domain.deliveryRequests.enums.Priority;
import be.kdg.operational.domain.tours.PostCode;
import be.kdg.operational.domain.tours.Tour;
import be.kdg.operational.domain.tours.TourStatus;
import be.kdg.operational.repository.PostCodesRepository;
import be.kdg.operational.repository.TourRepository;
import be.kdg.operational.services.PostCodeService;
import be.kdg.operational.services.ShipmentService;
import be.kdg.operational.services.TourService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class SimulationApplicationTests {
    @Autowired
    private ShipmentService shipmentService;
    private Tour tour = new Tour();
    private TourService tourService;
    private PostCodeService postCodeService;

    @MockBean
    private TourRepository tourRepository;
    private PostCodesRepository postCodesRepository;

    @Before
    private void setup() {
        var time = LocalDateTime.now().plusMinutes(16);
        var a = 1000;
        tour = new Tour();
        tourService.save(tour);
        List<PostCode> postcodes = new ArrayList<>();
        for (int j = 0; j <= 10; j++) {
            var postcode = new PostCode();
            postCodeService.save(postcode);
            postcode.setPostcode(a + j);
            postcode.setTour(tour);
            postCodeService.save(postcode);
            postcodes.add(postcode);
        }
        var size = 0;
        tour.setPostcodes(postcodes);
        tour.setRouteCapacity(size);
        tour.setPriority(Priority.SAMEDAY);
        tour.setStatus(TourStatus.AT_BASE);
        tour.setStartTime(time);
        tourService.save(tour);
    }

    @Test
    public void test() {
        shipmentService.CheckToursForShipment();
    }
}
