CREATE TABLE customer (
    id          INTEGER      NOT NULL AUTO_INCREMENT,
    name        VARCHAR(128) NOT NULL,
    reduction   INTEGER,
    credit      DOUBLE,
    currentBalance DOUBLE,
    PRIMARY KEY (id)
);

CREATE TABLE tours (
    id              INTEGER      NOT NULL AUTO_INCREMENT,
    routeCapacity   INTEGER     NOT NULL,
    startTime       DATETIME    NOT NULL,
    priority        INTEGER     NOT NULL,
    MAXCAPACITY     INTEGER     NOT NULL,
    status          INTEGER     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE tour (
    id              INTEGER      NOT NULL AUTO_INCREMENT,
    postCode        INTEGER     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE  priceData(
    id              INTEGER      NOT NULL AUTO_INCREMENT,
    sameDayPrice    DOUBLE NOT NULL,
    nextDayPrice    DOUBLE NOT NULL,
    weightFactor    DOUBLE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE VEHICLES(
    id              INTEGER      NOT NULL AUTO_INCREMENT,
    size            INTEGER NOT NULL,

)