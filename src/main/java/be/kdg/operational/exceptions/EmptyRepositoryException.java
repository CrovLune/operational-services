package be.kdg.operational.exceptions;

public class EmptyRepositoryException extends RepositoryOperationException {
    public EmptyRepositoryException(String message) {
        super(message);
    }
}
