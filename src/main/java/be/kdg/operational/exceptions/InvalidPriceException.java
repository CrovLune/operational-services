package be.kdg.operational.exceptions;

public class InvalidPriceException extends PaymentException {
    public InvalidPriceException(String message) {
        super(message);
    }
}
