package be.kdg.operational.exceptions;


public class RepositoryOperationException extends RuntimeException{
    public RepositoryOperationException(String message) {
        super(message);
    }
}
