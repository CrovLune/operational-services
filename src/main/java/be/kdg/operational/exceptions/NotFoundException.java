package be.kdg.operational.exceptions;


public class NotFoundException extends RepositoryOperationException {

    public NotFoundException(String message) {
        super(message);
    }
}
