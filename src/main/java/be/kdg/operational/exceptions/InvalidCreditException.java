package be.kdg.operational.exceptions;

public class InvalidCreditException extends PaymentException {
    public InvalidCreditException(String message) {
        super(message);
    }
}
