package be.kdg.operational.exceptions;

public class PaymentException extends Exception {
    public PaymentException(String message) {
        super(message);
    }
}
