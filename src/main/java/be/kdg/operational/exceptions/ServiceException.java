package be.kdg.operational.exceptions;


import lombok.Getter;
import org.springframework.http.HttpStatus;

public class ServiceException extends RuntimeException {
    @Getter
    private HttpStatus status;
    public ServiceException(String message,HttpStatus status) {
        super(message);
        this.status = status;
    }
}
