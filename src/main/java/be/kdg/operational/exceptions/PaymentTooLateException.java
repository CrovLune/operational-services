package be.kdg.operational.exceptions;

import javafx.concurrent.Service;

public class PaymentTooLateException extends PaymentException {
    public PaymentTooLateException(String message) {
        super(message);
    }
}
