package be.kdg.operational.configs;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Aleksey Zelenskiy
 * 11/7/2020.
 */
@Configuration
public class RabbitMQConfig {
    @Value("${shipmentQueue}")
    private String shipmentQueue;

    // TODO put queue name in application.properties
    @Bean
    public Queue paymentQueue() {
        return new Queue("payments", false);
    }
    @Bean
    public Queue takeInQueue() {
        return new Queue("takeins", false);
    }
    @Bean
    public Queue locationsQueue(){
        return new Queue("locations",false);
    }
    @Bean
    public Queue shipmentsQueue(){
        return new Queue(shipmentQueue,false);
    }
}
