// TODO: package structuur moet nog beter natuurlijk
package be.kdg.operational.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;

@Configuration
public class SimulationConfig {

    // deze annotatie zorgt voor dat de Pojo ObjectMapper een instantie wordt in de Spring Container.
    // Zo'n instantie noemen we een Spring Bean
    @Bean
    ObjectMapper objectMapper(){
        // ObjectMapper is een Pojo
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        objectMapper.setDateFormat(df);
        return objectMapper;
    }

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }
}
