package be.kdg.operational.controllers;

import be.kdg.operational.controllers.communication.in.message.TakeInReceiver;
import be.kdg.operational.services.ParcelService;
import be.kdg.operational.services.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.



@Slf4j
public class ParcelController {
    @Value("${maxPaymentChecks}")
    private int maxChecks;
    private final ParcelService service;
    private final PaymentService paymentService;
    private final TakeInReceiver takeInReceiver;

    public ParcelController(ParcelService parcelService, PaymentService paymentService, TakeInReceiver takeInReceiver) {
        this.service = parcelService;
        this.paymentService = paymentService;
        this.takeInReceiver = takeInReceiver;
    }


    @Scheduled(fixedDelay = 20000, initialDelay = 10000)
    public void checkDelayedParcels() throws NullPointerException {

        System.out.println("----------------------------------------------------------------------------------------------------------------------");
        log.warn(String.format("DELAYED Parcels are being checked now!"));
        System.out.println("----------------------------------------------------------------------------------------------------------------------");

        try {
            var delayedParcels = service.getAllDelayedParcels();
            for (var parcel : delayedParcels) {
                System.out.println("----------------------------------------------------------------------------------------------------------------------");
                log.warn(String.format("DELAYED: Checking ParcelID [%s]!", parcel.getId()));
                System.out.println("----------------------------------------------------------------------------------------------------------------------");
                if (parcel.getChecksPerformed() < maxChecks) {
                    //? Find Payment associated to Parcel
                    var isParcelPaid = paymentService.isParcelPaid(parcel);
                    if (isParcelPaid) {
                        System.out.println("----------------------------------------------------------------------------------------------------------------------");
                        log.warn(String.format("DELAYED: Assigning tour NOW to ParcelID [%s]", parcel.getId()));
                        System.out.println("----------------------------------------------------------------------------------------------------------------------");
                        takeInReceiver.assignTour(parcel);
                        System.out.println("----------------------------------------------------------------------------------------------------------------------");
                        log.warn(String.format("DELAYED: Tour is assigned Successfully!", parcel.getId()));
                        System.out.println("----------------------------------------------------------------------------------------------------------------------");
                    }
                    parcel.setChecksPerformed(parcel.getChecksPerformed() + 1);
                    service.save(parcel);
                } else {
                    service.setParcelRequiresManualAction(parcel);
                }
            }
        } catch (NullPointerException exception) {
            log.info("checkDelayedParcels(): No Delayed Parcels Found.");
        }
    }

    @Scheduled(fixedDelay = 20000, initialDelay = 10000)
    public void checkPaidParcelsWithNoTour() throws NullPointerException {

        System.out.println("----------------------------------------------------------------------------------------------------------------------");
        log.warn(String.format("NO TOUR Parcels are being checked now!"));
        System.out.println("----------------------------------------------------------------------------------------------------------------------");

        try {
            var noTour = service.getParcelsWithNoTour();
            for (var parcel : noTour) {
                System.out.println("----------------------------------------------------------------------------------------------------------------------");
                log.warn(String.format("NO TOUR: Checking ParcelID [%s]!", parcel.getId()));
                System.out.println("----------------------------------------------------------------------------------------------------------------------");
                if (parcel.getChecksPerformed() < maxChecks) {
                    //? Find Payment associated to Parcel

                    System.out.println("----------------------------------------------------------------------------------------------------------------------");
                    log.warn(String.format("NO TOUR: Assigning tour NOW to ParcelID [%s]", parcel.getId()));
                    System.out.println("----------------------------------------------------------------------------------------------------------------------");
                    takeInReceiver.assignTour(parcel);
                    System.out.println("----------------------------------------------------------------------------------------------------------------------");
                    log.warn(String.format("NO TOUR: Tour is assigned Successfully!", parcel.getId()));
                    System.out.println("----------------------------------------------------------------------------------------------------------------------");

                    parcel.setChecksPerformed(parcel.getChecksPerformed() + 1);
                    service.save(parcel);
                } else {
                    service.setParcelRequiresManualAction(parcel);
                }
            }
        } catch (NullPointerException exception) {
            log.info("checkDelayedParcels(): No Delayed Parcels Found.");
        }
    }
}
*/