package be.kdg.operational.controllers;

import be.kdg.operational.exceptions.EmptyRepositoryException;
import be.kdg.operational.exceptions.NotFoundException;
import be.kdg.operational.exceptions.RepositoryOperationException;
import be.kdg.operational.exceptions.ServiceException;
import be.kdg.operational.controllers.dtos.DeliveryRequestDTO;
import be.kdg.operational.controllers.dtos.ResponseDTO;
import be.kdg.operational.services.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;

@RestController
@Slf4j
@RequestMapping("/api")
public class DeliveryRequestController {
    private final RequestService service;

    public DeliveryRequestController(RequestService service) {
        this.service = service;
    }

    @PostMapping("/request")
    public ResponseEntity<ResponseDTO> response(@RequestBody DeliveryRequestDTO dto, HttpServletResponse response) {
        try {
            log.info("Receiver DeliveryRequest");
            return new ResponseEntity<ResponseDTO>(service.generateResponse(dto), HttpStatus.ACCEPTED);
        } catch (ServiceException ex) {
            log.error("caught Exception: " +ex.getMessage());
            return new ResponseEntity<ResponseDTO>(new ResponseDTO(), ex.getStatus());
        }

    }

}
