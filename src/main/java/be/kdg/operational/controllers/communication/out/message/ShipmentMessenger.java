package be.kdg.operational.controllers.communication.out.message;

import be.kdg.operational.controllers.dtos.ShipmentDTO;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Aleksey Zelenskiy
 * 11/7/2020.
 */
@Component
@Slf4j
public class ShipmentMessenger {
    @Value("${shipmentQueue}")
    private String shipmentQueue;
    private final RabbitTemplate messenger;

    public ShipmentMessenger(RabbitTemplate messenger) {
        this.messenger = messenger;
    }

    public void sendMessage(ShipmentDTO dto) {
        messenger.convertAndSend(shipmentQueue, dto);
        log.info("sendMessage - TakeIn Sent!");
    }
}
