package be.kdg.operational.controllers.communication.in.message;

import be.kdg.operational.domain.locations.Location;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
@Component
public class LocationReceiver {
    @RabbitListener(queues = "locations")
    public void listen(Location message) {
        // do something with this message...
        System.out.println("----------------------------------------------------------------------------------------------");
        System.out.println(message);
        System.out.println("----------------------------------------------------------------------------------------------");
    }
}
