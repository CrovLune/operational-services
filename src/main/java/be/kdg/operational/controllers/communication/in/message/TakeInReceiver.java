package be.kdg.operational.controllers.communication.in.message;

import be.kdg.operational.controllers.dtos.ParcelTakeInDTO;
import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.tours.Tour;
import be.kdg.operational.domain.tours.TourStatus;
import be.kdg.operational.services.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@Slf4j
public class TakeInReceiver {
    private TakeInService takeInService;

    public TakeInReceiver(TakeInService takeInService) {
        this.takeInService = takeInService;
    }

    @RabbitListener(queues = "takeins")
    public void listen(ParcelTakeInDTO dto) {
        try {
            log.info("received takein for Parcel " + dto.getParcelId());
            takeInService.handleTakeIn(dto);
        } catch (Exception e) {
            log.error("throw Exception in TakeinReceiver: "+e.getMessage());
        }
    }
}