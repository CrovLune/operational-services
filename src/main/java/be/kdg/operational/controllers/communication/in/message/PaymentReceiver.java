package be.kdg.operational.controllers.communication.in.message;

import be.kdg.operational.domain.payments.Payment;
import be.kdg.operational.services.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PaymentReceiver {
    private final PaymentService paymentService;

    public PaymentReceiver(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @RabbitListener(queues = "payments")
    public void listen(Payment payment) {
        try {
            log.info("received payment for Parcel  " + payment.getParcelID() + " - €" + payment.getAmount());
            paymentService.handlePayment(payment);
        } catch (Exception ex) {
            log.error("throw Exception in TakeinReceiver: " + ex.getMessage());
        }
    }


}