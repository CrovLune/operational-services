package be.kdg.operational.controllers.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
public class ParcelsDTO {
    @Setter
    @Getter
    @JsonProperty()
//    @JsonDeserialize(using = NumberDeserializers.LongDeserializer.class)
    private List<Long> id;

    public ParcelsDTO() {
    }

    public ParcelsDTO(List<Long> id) {
        this.id = id;
    }

}
