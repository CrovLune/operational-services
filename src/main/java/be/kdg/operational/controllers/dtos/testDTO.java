//package be.kdg.operational.controllers.dtos;
//
//import be.kdg.operational.domain.deliveryRequests.Response;
//import be.kdg.operational.domain.deliveryRequests.enums.NotHomeAction;
//import be.kdg.operational.domain.deliveryRequests.enums.Priority;
//import com.opencsv.bean.CsvBindByName;
//import lombok.Getter;
//import lombok.Setter;
//
//import javax.persistence.*;
//import java.util.Objects;
//
///**
// * Aleksey Zelenskiy
// * 11/5/2020.
// */
//public class testDTO {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Getter
//    @Setter
//    private long id;
//    @CsvBindByName
//    @Getter
//    @Setter
//    private long customerId;
//    @Getter
//    @Setter
//    private int weight;
//    @Getter
//    @Setter
//    private int size;
//    @Getter
//    @Setter
//    private Priority priority;
//    @CsvBindByName
//    @Getter
//    @Setter
//    private String address;
//    @Getter
//    @Setter
//    private NotHomeAction action;
//
//    //? Add relation to Delivery Request Response
//    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @Getter
//    @Setter
//    private Response response;
//
//    public testDTO() {
//    }
//
//    public testDTO(long customerId, int weight, int size, Priority priority, String address, NotHomeAction action) {
//        this.customerId = customerId;
//        this.weight = weight;
//        this.size = size;
//        this.priority = priority;
//        this.address = address;
//        this.action = action;
//    }
//
//    @Override
//    public String toString() {
//        return "DeliveryRequest{" +
//                "id=" + id +
//                ", customerId=" + customerId +
//                ", weight=" + weight +
//                ", size=" + size +
//                ", priority=" + priority +
//                ", address='" + address + '\'' +
//                ", action=" + action +
//                ", response=" + response +
//                '}';
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        testDTO testDTO = (testDTO) o;
//        return customerId == testDTO.customerId &&
//                weight == testDTO.weight &&
//                size == testDTO.size &&
//                priority == testDTO.priority &&
//                Objects.equals(address, testDTO.address) &&
//                action == testDTO.action;
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(customerId, weight, size, priority, address, action);
//    }
//}
