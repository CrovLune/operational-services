package be.kdg.operational.controllers.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Aleksey Zelenskiy
 * 11/7/2020.
 */
public class ResponseDTO {
    @Getter
    @Setter
    private double price;
    @Getter
    @Setter
    private long parcelId;
    @Getter
    @Setter
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate validUntil;

    public ResponseDTO() {
    }

    public ResponseDTO(double price, long parcelId, LocalDate validUntil) {
        this.price = price;
        this.parcelId = parcelId;
        this.validUntil = validUntil;
    }
}
