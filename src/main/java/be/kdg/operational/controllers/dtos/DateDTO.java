package be.kdg.operational.controllers.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
@Component
public class DateDTO {
    @Setter
    @Getter
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate date;

    public DateDTO() {
    }

    public DateDTO(LocalDate date) {
        this.date = date;
    }

    public int getYear(){
        return this.date.getYear();
    }
    public int getMonth(){
        return this.date.getMonthValue();
    }
    public int getDay(){
        return this.date.getDayOfMonth();
    }
}
