package be.kdg.operational.controllers.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Aleksey Zelenskiy
 * 11/7/2020.
 */
public class ShipmentDTO {
    @Setter
    @Getter
    private long tourId;
    @Getter
    @Setter
    private List<Long> parcelIds;
    @Getter
    @Setter
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime startDateTime;

    public ShipmentDTO() {
    }

    public ShipmentDTO(long tourId, List<Long> parcelIds, LocalDateTime startDateTime) {
        this.tourId = tourId;
        this.parcelIds = parcelIds;
        this.startDateTime = startDateTime;
    }
}
