package be.kdg.operational.controllers.dtos;

import be.kdg.operational.domain.deliveryRequests.enums.NotHomeAction;
import be.kdg.operational.domain.deliveryRequests.enums.Priority;
import lombok.Getter;
import lombok.Setter;

/**
 * Aleksey Zelenskiy
 * 11/7/2020.
 */
public class DeliveryRequestDTO {
    @Setter
    @Getter
    private long customerId;
    @Setter
    @Getter
    private int size;
    @Setter
    @Getter
    private int weight;
    @Setter
    @Getter
    private Priority priority;
    @Setter
    @Getter
    private String address;
    @Setter
    @Getter
    private NotHomeAction action;

    public DeliveryRequestDTO(long customerId, int size, int weight, Priority priority, String address, NotHomeAction action) {
        this.customerId = customerId;
        this.size = size;
        this.weight = weight;
        this.priority = priority;
        this.address = address;
        this.action = action;
    }

    public DeliveryRequestDTO() {
    }
}
