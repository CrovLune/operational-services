package be.kdg.operational.controllers.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.Getter;
import lombok.Setter;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Date;


public class DeliveryRequestResponseDTO {
    @Getter
    @Setter
    private double price;
    @Getter
    @Setter
    private int parcelId;
    @Getter
    @Setter
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate validUntil;


    public DeliveryRequestResponseDTO(double price, int parcelId, LocalDate validUntil) {
        this.price = price;
        this.parcelId = parcelId;
        this.validUntil = validUntil;
    }



}
