package be.kdg.operational.controllers.dtos;

import be.kdg.operational.domain.deliveryRequests.enums.NotHomeAction;
import lombok.Getter;
import lombok.Setter;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
public class ParcelDTO {
    @Getter
    @Setter
    private long parcelId;
    @Getter
    @Setter
    private NotHomeAction action;
    @Getter
    @Setter
    private String address;

    public ParcelDTO() {
    }

    public ParcelDTO(long parcelId, NotHomeAction action, String address) {
        this.parcelId = parcelId;
        this.action = action;
        this.address = address;
    }

    @Override
    public String toString() {
        return "ParcelDTO{" +
                "parcelId=" + parcelId +
                ", action=" + action +
                ", address='" + address + '\'' +
                '}';
    }
}
