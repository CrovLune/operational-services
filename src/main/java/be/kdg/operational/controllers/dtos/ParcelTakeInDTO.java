package be.kdg.operational.controllers.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
public class ParcelTakeInDTO {
    @Getter
    @Setter
    private long parcelId;
    @Getter
    @Setter
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime timestamp;

    public ParcelTakeInDTO() {
    }

    public ParcelTakeInDTO(long parcelId, LocalDateTime timestamp) {
        this.parcelId = parcelId;
        this.timestamp = timestamp;
    }
}