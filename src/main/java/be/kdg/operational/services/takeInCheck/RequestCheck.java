package be.kdg.operational.services.takeInCheck;

import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.exceptions.RepositoryOperationException;
import be.kdg.operational.services.CustomerService;
import be.kdg.operational.services.RequestService;
import org.springframework.stereotype.Component;


@Component
public class RequestCheck implements TakeInCheck {
    private RequestService requestService;

    public RequestCheck(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public TakeInCheckResult check(Parcel parcel) {
        try {
            var request = requestService.readById(parcel.getDeliveryrequest().getId());
            request.setParcelArrivedAtWH(true);
            requestService.save(request);
        } catch (RepositoryOperationException ex) {
            return new TakeInCheckResult(false,String.format("RequestCheck: [%s]",ex.getMessage()));
        }
        return new TakeInCheckResult(true, "Passed RequestCheck");
    }
}
