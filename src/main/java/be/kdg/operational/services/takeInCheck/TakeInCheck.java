package be.kdg.operational.services.takeInCheck;

import be.kdg.operational.domain.parcels.Parcel;

public interface TakeInCheck {
    TakeInCheckResult check(Parcel parcel);
}
