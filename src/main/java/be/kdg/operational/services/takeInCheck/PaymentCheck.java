package be.kdg.operational.services.takeInCheck;

import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.services.CustomerService;
import be.kdg.operational.services.ParcelService;
import be.kdg.operational.services.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
public class PaymentCheck implements TakeInCheck {

    PaymentService paymentService;
    ParcelService parcelService;
    CustomerService customerService;

    public PaymentCheck(PaymentService paymentService, ParcelService parcelService, CustomerService customerService) {
        this.paymentService = paymentService;
        this.parcelService = parcelService;
        this.customerService = customerService;
    }

    @Override
    public TakeInCheckResult check(Parcel parcel) {
        boolean isOK = true;
        String message = "";
        try {
            var customer = parcel.getDeliveryrequest().getCustomer();

            //? Client
            switch (customer.getType()) {
                case COMPANY:
                    customerService.reduceBalance(customer, parcel.getPrice());
                default: //client payment check
                    var payment = paymentService.findPaymentByParcel(parcel.getId());
                    isOK = payment.getAmount() == parcel.getPrice();
                    message = String.format("payment: [%f] - price: [%f]", payment.getAmount(), parcel.getPrice());
            }
            if (isOK) {
                parcelService.setParcelIsPaid(parcel);
                return new TakeInCheckResult(true, "Passed PaymentCheck");
            } else {
                return new TakeInCheckResult(false, message);
            }
        }catch (Exception ex){
            return new TakeInCheckResult(false, "PaymentCheck Exception: "+ex.getMessage());
        }
    }
}
