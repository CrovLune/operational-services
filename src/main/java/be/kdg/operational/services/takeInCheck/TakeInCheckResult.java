package be.kdg.operational.services.takeInCheck;

import lombok.Getter;

public class TakeInCheckResult {
    @Getter
    boolean passed;
    @Getter
    String message;

    public TakeInCheckResult(boolean passed, String message) {
        this.passed = passed;
        this.message = message;
    }
}
