package be.kdg.operational.services.takeInCheck;

import be.kdg.operational.domain.parcels.Parcel;
import org.springframework.stereotype.Component;

@Component
public class ValidUntilCheck implements TakeInCheck{
    @Override
    public TakeInCheckResult check(Parcel parcel) {
        var isOk = parcel.getReceivedatwhtime().toLocalDate().isBefore(parcel.getValidUntil())
                || parcel.getReceivedatwhtime().toLocalDate().isEqual(parcel.getValidUntil());
        if (isOk)
            return new TakeInCheckResult(true,"Passed ValidUntilCheck");
        else
            return new TakeInCheckResult(false,"Request for parcel is not valid anymore");

    }
}
