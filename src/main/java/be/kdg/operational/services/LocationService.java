package be.kdg.operational.services;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.locations.Location;
import be.kdg.operational.repository.LocationRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Service
@Transactional
@Slf4j
public class LocationService implements ServicesInterface<Location> {
    private final LocationRepository repo;

    public LocationService(LocationRepository repo) {
        this.repo = repo;
    }

    @Override
    public Location save(Location o) {
        return repo.save(o);
    }

    @Override
    public Location readById(long id) throws NullPointerException {
        Location location = null;
        try {
            if (repo.findById(id).isPresent())
                location = repo.findById(id).get();
        } catch (NullPointerException exception) {
            log.error(String.format("readById(): Couldn't find Location: [%s].", id));
        }
        return location;
    }

    @Override
    public List<Location> readAll() throws NullPointerException {
        List<Location> list = null;
        try {
            if (!repo.findAll().isEmpty())
                list = repo.findAll();
        } catch (NullPointerException exception) {
            log.error("readAll(): Couldnt find any Location.");
        }
        return list;
    }

    @Override
    public void delete(Location o) {
        repo.delete(o);
    }

}
