package be.kdg.operational.services;


import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.parcels.enums.ParcelDeliveryStatus;
import be.kdg.operational.domain.parcels.enums.ParcelPaymentStatus;
import be.kdg.operational.exceptions.EmptyRepositoryException;
import be.kdg.operational.exceptions.NotFoundException;
import be.kdg.operational.exceptions.RepositoryOperationException;
import be.kdg.operational.exceptions.ServiceException;
import be.kdg.operational.controllers.dtos.DeliveryRequestDTO;
import be.kdg.operational.controllers.dtos.ResponseDTO;
import be.kdg.operational.domain.deliveryRequests.Request;
import be.kdg.operational.domain.deliveryRequests.enums.DeliveryRequestState;
import be.kdg.operational.repository.DeliveryRequestRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import be.kdg.operational.services.priceCalculation.PriceCalculationDecider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;


@Service
@Transactional
@Slf4j
public class RequestService implements ServicesInterface<Request> {
    private final DeliveryRequestRepository repo;
    private final CustomerService customerService;
    private final PriceCalculationDecider calculationDecider;
    private final ParcelService parcelService;

    public RequestService(DeliveryRequestRepository deliveryRequestRepository, CustomerService customerService, PriceCalculationDecider calculationDecider, ParcelService parcelService) {
        this.repo = deliveryRequestRepository;
        this.customerService = customerService;
        this.calculationDecider = calculationDecider;
        this.parcelService = parcelService;
    }


    public ResponseDTO generateResponse(DeliveryRequestDTO dto) throws ServiceException {
        try {
            // Find Customer in DB.
            var customer = customerService.readById(dto.getCustomerId());
            //from DTO to domain object
            var requestTemp = new Request(dto.getWeight(), dto.getSize(), dto.getPriority(), dto.getAddress(), dto.getAction(), null, DeliveryRequestState.NOT_PAID, null);
            var request = this.save(requestTemp);
            //? Calculate price for Delivery.
            var price = calculationDecider.calculate(request, customer);

            var parcelTemp = new Parcel(price, LocalDate.now().plusDays(7), ParcelDeliveryStatus.NOT_DELIVERED, ParcelPaymentStatus.NOT_PAID);
            //TODO FIX Column "DELIVERYREQUEST_ID" not found; SQL statement:
            // only on first time
            // fix: start simulation before operational
            //System.out.println("please work");
            var parcel = parcelService.save(parcelTemp);
            // request -> parcel
            // request -> customer
            request.setParcel(parcel);
            request.setCustomer(customer);

            parcel.setDeliveryrequest(request);
            this.save(request);
            parcelService.save(parcelTemp);



            var response = new ResponseDTO(parcel.getPrice(), parcel.getId(), parcel.getValidUntil());

            return response;

        } catch (NotFoundException | EmptyRepositoryException ex) {
            throw new ServiceException(String.format("generateResponse(): [%s]", ex.getMessage()), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            throw new ServiceException(String.format("generateResponse(): [%s]", ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public Request save(Request o) throws RepositoryOperationException {
        try {
            return repo.save(o);
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Request.save(): [%s]", ex.getMessage()));
        }
    }

    @Override
    public Request readById(long id) throws RepositoryOperationException {
        try {
            var found = repo.findById(id);
            if (!found.isPresent())
                throw new NotFoundException(String.format("Couldn't find Request with ID: [%s]", id));
            return found.get();
        } catch (NotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Request.readById(): [%s]", ex.getMessage()));
        }
    }

    public List<Request> readAll() throws RepositoryOperationException {
        try {
            var list = repo.findAll();
            if (list.isEmpty())
                throw new EmptyRepositoryException("No entries in DeliveryRequest Repository");
            else
                return list;
        } catch (EmptyRepositoryException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Request.readAll(): [%s]", ex.getMessage()));
        }
    }


    public void delete(Request o) throws RepositoryOperationException {
        try {
            var found = this.readById(o.getId());
            repo.delete(found);
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Request.delete(): [%s]", ex.getMessage()));
        }
    }


}
