
package be.kdg.operational.services;


import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.deliveryRequests.Request;
import be.kdg.operational.domain.payments.Payment;
import be.kdg.operational.exceptions.EmptyRepositoryException;
import be.kdg.operational.exceptions.InvalidCreditException;
import be.kdg.operational.exceptions.NotFoundException;
import be.kdg.operational.exceptions.RepositoryOperationException;
import be.kdg.operational.repository.CustomerRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
@Service
@Transactional
@Slf4j
public class CustomerService implements ServicesInterface<Customer> {
    private final CustomerRepository repo;

    public CustomerService(CustomerRepository repo) {
        this.repo = repo;
    }

    public Customer save(Customer o) throws RepositoryOperationException {
        try {
            log.info("saving Customer " + o.getName() + "(" + o.getId() + ")");
            return repo.save(o);
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Customer.save(): [%s]", ex.getMessage()));
        }
    }

    @Override
    public Customer readById(long id) throws RepositoryOperationException {
        try {
            log.info("trying to find customer " + id);
            var found = repo.findById(id);
            if (!found.isPresent())
                throw new NotFoundException(String.format("Couldn't find Customer with ID: [%s]", id));
            return found.get();
        } catch (NotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Customer.readById(): [%s]", ex.getMessage()));
        }
    }

    public List<Customer> readAll() throws RepositoryOperationException {
        try {
            var list = repo.findAll();
            if (list.isEmpty())
                throw new EmptyRepositoryException("No entries in DeliveryCustomer Repository");
            else
                return list;
        } catch (EmptyRepositoryException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Customer.readAll(): [%s]", ex.getMessage()));
        }
    }


    public void delete(Customer o) throws RepositoryOperationException {
        try {
            log.info("trying to delete customer " + o.getName() + "(" + o.getId() + ")");
            var found = this.readById(o.getId());
            if (found != null)
                throw new NotFoundException("Could not delete customer " + o.getName() + "(" + o.getId() + "). Customer does not exists");
            repo.delete(found);
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Request.delete(): [%s]", ex.getMessage()));
        }
    }


    public void increaseCompanyBalance(Customer company, Payment payment) throws Exception {
        //? change balance
        company.setCurrentbalance(company.getCurrentbalance() + payment.getAmount());
        //? save changes
        this.save(company);
    }

    public double checkBalance(Customer company, double price) throws InvalidCreditException {
        var balanceAfterReduction = company.getCurrentbalance() - price;
        log.info("Company " + company.getName() + " (" + company.getId() + ") will have " + balanceAfterReduction + " / " + company.getCredit() + " after this transaction");
        //if company has enough credit for purchase
        if (company.getCredit() > balanceAfterReduction) {
            throw new InvalidCreditException(String.format("balance: [%f] - price: [%f] < credit: [%f] ", company.getCurrentbalance(), price, company.getCredit()));
        }
        return balanceAfterReduction;
    }

    public void reduceBalance(Customer company, double price) throws InvalidCreditException {
        var newBalance = this.checkBalance(company, price);

        company.setCurrentbalance(newBalance);
        this.save(company);
    }


    public boolean checkCredit(Customer company) {
        return company.getCredit() < company.getCurrentbalance();
    }
}
