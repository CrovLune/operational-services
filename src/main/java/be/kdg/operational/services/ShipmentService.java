
package be.kdg.operational.services;

import be.kdg.operational.controllers.communication.out.message.ShipmentMessenger;
import be.kdg.operational.controllers.dtos.ShipmentDTO;
import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.tours.Tour;
import be.kdg.operational.domain.tours.TourStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */

@Service
@Slf4j
public class ShipmentService {

    private final Comparator<Parcel> sortOnTime = (o1, o2) -> {
        var a = o1.getReceivedatwhtime().isBefore(o2.getReceivedatwhtime());
        return a ? 1 : 0;
    };

    private ShipmentMessenger shipmentMessenger;
    private TourService tourService;
    private VehicleService vehicleService;
    private ParcelService parcelService;
    public ShipmentService(ShipmentMessenger shipmentMessenger, TourService tourService, VehicleService vehicleService,ParcelService parcelService) {
        this.shipmentMessenger = shipmentMessenger;
        this.tourService = tourService;
        this.vehicleService = vehicleService;
        this.parcelService = parcelService;
    }

    public void sendShipment(ShipmentDTO parcel) {
        shipmentMessenger.sendMessage(parcel);
    }


    @Scheduled(fixedDelay = 5000000)
    public void CheckToursForShipment() {
        try {
            var tours = tourService.getReadyForShipmentTours(LocalDate.now());
            if (!tours.isEmpty()) {
                for (var tour : tours) {
                    //sort parcels on time
                    var sorted = tour.getParcels()
                            .stream()
                            .sorted(sortOnTime)
                            .collect(Collectors.toList());
                    List<Long> ids = new ArrayList<>();
                    for (var x : sorted) {
                        ids.add(x.getId());
                    }
                    var shipment = new ShipmentDTO(tour.getId(), ids, tour.getStartTime());
                    this.assignTourToVehicle(tour);
                    this.sendShipment(shipment);
                    tour.setStatus(TourStatus.ON_DELIVERY);
                    tourService.save(tour);
                }
            }
        } catch (Exception exception) {
            log.info("CheckToursForShipment(): " + exception.getMessage());
        }
    }

    public void assignTourToVehicle(Tour tour) {
        var vehicle = vehicleService.getVehicleForTour(tour);
        vehicle.setTour(tour);
        tour.setVehicle(vehicle);

        var parcels = tour.getParcels();
        for (var parcel : parcels) {
            parcel.setVehicle(vehicle);
            parcelService.save(parcel);
        }
        vehicleService.save(vehicle);
        tourService.save(tour);
    }

}
