package be.kdg.operational.services.paymentHandlers;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.payments.Payment;

public interface PaymentHandler {
    void handle(Payment payment, Customer customer) throws Exception;
}
