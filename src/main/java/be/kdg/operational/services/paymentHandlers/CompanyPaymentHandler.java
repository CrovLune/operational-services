package be.kdg.operational.services.paymentHandlers;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.payments.Payment;
import be.kdg.operational.services.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CompanyPaymentHandler implements PaymentHandler {
    private CustomerService customerService;

    public CompanyPaymentHandler(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public void handle(Payment payment, Customer company) throws Exception {
        log.info("raising credit of company " + company.getName() +"("+company.getId()+") with " + payment.getAmount());
        customerService.increaseCompanyBalance(company, payment);
    }
}
