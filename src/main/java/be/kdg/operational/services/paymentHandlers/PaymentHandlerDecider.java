package be.kdg.operational.services.paymentHandlers;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.payments.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PaymentHandlerDecider {
    private ClientPaymentHandler clientPaymentHandler;
    private CompanyPaymentHandler companyPaymentHandler;

    public PaymentHandlerDecider(ClientPaymentHandler clientPaymentHandler, CompanyPaymentHandler companyPaymentHandler) {
        this.clientPaymentHandler = clientPaymentHandler;
        this.companyPaymentHandler = companyPaymentHandler;
    }


    public void handle(Payment payment, Customer customer) throws Exception{
        PaymentHandler paymentHandler;
        switch (customer.getType()){
            case COMPANY: paymentHandler = companyPaymentHandler; break;
            default: paymentHandler = clientPaymentHandler;
        }
        paymentHandler.handle(payment,customer);
    }
}
