package be.kdg.operational.services.paymentHandlers;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.parcels.enums.ParcelPaymentStatus;
import be.kdg.operational.domain.payments.Payment;
import be.kdg.operational.exceptions.InvalidPriceException;
import be.kdg.operational.exceptions.PaymentTooLateException;
import be.kdg.operational.services.ParcelService;
import org.springframework.stereotype.Component;

@Component
public class ClientPaymentHandler implements PaymentHandler {
    private ParcelService parcelService;

    public ClientPaymentHandler(ParcelService parcelService) {
        this.parcelService = parcelService;
    }

    @Override
    public void handle(Payment payment, Customer client) throws Exception {
        Parcel parcel = null;
        try {

            //throws NotFound error
            parcel = parcelService.readById(payment.getParcelID());

            var onTime = this.checkDate(payment, parcel);
            if (!onTime)
                throw new PaymentTooLateException("Payment too late for parcel: "+ payment.getTimestamp().toLocalDate() + ", Valid until:" + parcel.getValidUntil().toString());
            //if price doesn't match
            if (parcel.getPrice() != payment.getAmount())
                throw new InvalidPriceException("price doesn't match: Parcel:" + parcel.getPrice() + " | Payment: " + payment.getAmount());

            parcel.setPaymentstatus(ParcelPaymentStatus.PAID);

        } catch (Exception e) {
            if (parcel != null)
                parcel.setPaymentstatus(ParcelPaymentStatus.MANUAL_ACTION);
            throw e;
        } finally {
            if (parcel != null)
                parcelService.save(parcel);
        }
    }

    private boolean checkDate(Payment payment, Parcel parcel) {
        return payment.getTimestamp().toLocalDate().isBefore(parcel.getValidUntil());
    }
}
