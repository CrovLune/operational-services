package be.kdg.operational.services;

import be.kdg.operational.controllers.dtos.ParcelTakeInDTO;
import be.kdg.operational.domain.deliveryRequests.Request;
import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.parcels.enums.ParcelDeliveryStatus;
import be.kdg.operational.domain.parcels.enums.ParcelPaymentStatus;
import be.kdg.operational.exceptions.NotFoundException;
import be.kdg.operational.exceptions.RepositoryOperationException;
import be.kdg.operational.repository.ParcelRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Service
@Transactional
@Slf4j
public class ParcelService implements ServicesInterface<Parcel> {
    private final ParcelRepository repo;

    @Autowired
    public ParcelService(ParcelRepository repo) {
        this.repo = repo;
    }


    @Override
    public Parcel save(Parcel o) {
        return repo.save(o);
    }

    @Override
    public Parcel readById(long id) throws RepositoryOperationException {
        try {
            log.info("finding Parcel " + id);
            var foundParcel = repo.findById(id);
            if (foundParcel.isPresent())
                return repo.findById(id).get();
            else
                throw new NotFoundException(String.format("readById(): Couldn't find ParcelID: [%s].", id));

        } catch (NotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("readById(): [%s]", ex.getMessage()));
        }

    }

    @Override
    public List<Parcel> readAll() throws NullPointerException {
        List<Parcel> list = null;
        try {
            if (!repo.findAll().isEmpty())
                list = repo.findAll();
        } catch (NullPointerException exception) {
            log.error("readAll(): Couldnt find any Parcel.");
        }
        return list;
    }

    @Override
    public void delete(Parcel o) {
        repo.delete(o);
    }

    public int getPostalCodeFromParcel(Parcel parcel) {
        var address  =parcel.getDeliveryrequest().getAddress();
        var postcodeString = address.substring(address.length() - 4);
        var postcode = Integer.parseInt(postcodeString);
        return postcode;
    }

    public void setParcelIsPaid(Parcel parcel) {
        parcel.setPaymentstatus(ParcelPaymentStatus.PAID);
        this.save(parcel);
    }

    public void setParcelIsNotPaid(Parcel parcel) {
        parcel.setPaymentstatus(ParcelPaymentStatus.NOT_PAID);
        this.save(parcel);
    }

    public void setParcelStatusDelayed(Parcel parcel) {
        parcel.setIsdelayed(true);
        this.save(parcel);
    }

    public void setParcelRequiresManualAction(Parcel parcel) {
        parcel.setPaymentstatus(ParcelPaymentStatus.MANUAL_ACTION);
        this.save(parcel);
    }

    public List<Parcel> getAllDelayedParcels() throws NullPointerException {
        List<Parcel> list = null;
        try {
            var found = this.readAll()
                    .stream()
                    .filter(Parcel::isIsdelayed)
                    .collect(Collectors.toList());
            if (!found.isEmpty())
                list = found;

        } catch (NullPointerException exception) {
            log.error("getAllDelayedParcels(): Couldn't find any Parcels with ParcelPaymentStatus == DELAYED");
        }
        return list;
    }


    public void setParcelArrivalTime(Parcel parcel, ParcelTakeInDTO dto) {
        parcel.setReceivedatwhtime(dto.getTimestamp());
        this.save(parcel);
    }

    public List<Parcel> getParcelsWithNoTour() throws NullPointerException {
        List<Parcel> list = null;
        try {
            var found = this.readAll()
                    .stream()
                    .filter(x -> x.getTour() == null && x.getPaymentstatus() == ParcelPaymentStatus.PAID)
                    .collect(Collectors.toList());
            if (!found.isEmpty())
                list = found;

        } catch (NullPointerException exception) {
            log.error("getAllDelayedParcels(): Couldn't find any Parcels with no tours & ParcelPaymentStatus == PAID");
        }
        return list;
    }
}
