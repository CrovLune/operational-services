package be.kdg.operational.services;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.tours.PostCode;
import be.kdg.operational.repository.PostCodesRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Aleksey Zelenskiy
 * 11/6/2020.
 */
@Service
@Transactional
@Slf4j
public class PostCodeService implements ServicesInterface<PostCode> {
    private final PostCodesRepository repo;

    public PostCodeService(PostCodesRepository repo) {
        this.repo = repo;
    }

    @Override
    public PostCode save(PostCode o) {
        return repo.save(o);
    }

    @Override
    public PostCode readById(long id) throws NullPointerException {
        PostCode postCode = null;
        try {
            if (repo.findById(id).isPresent())
                postCode = repo.findById(id).get();
        } catch (NullPointerException exception) {
            log.error(String.format("readById(): Couldn't find PostCodeID: [%s].", id));
        }
        return postCode;
    }

    @Override
    public List<PostCode> readAll() throws NullPointerException {
        List<PostCode> list = null;
        try {
            if (!repo.findAll().isEmpty())
                list = repo.findAll();
        } catch (NullPointerException exception) {
            log.error("readAll(): Couldnt find any PostCode.");
        }
        return list;
    }

    @Override
    public void delete(PostCode o) {
        repo.delete(o);
    }

    public PostCode getPostcodeByInt(int postcode) throws NullPointerException {
        PostCode pc = null;
        try {
            var found = this.readAll()
                    .stream()
                    .filter(x -> x.getPostcode() == postcode)
                    .collect(Collectors.toList());
            if (!found.isEmpty())
                pc = found.get(0);
        } catch (NullPointerException exception) {
            log.error(String.format("getPostcodeByInt(): Couldn't find any PostCode with postcode: [%s].", postcode));
        }
        return pc;
    }
}
