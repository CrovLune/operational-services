package be.kdg.operational.services;

import be.kdg.operational.domain.payments.Payment;
import be.kdg.operational.exceptions.EmptyRepositoryException;
import be.kdg.operational.exceptions.NotFoundException;
import be.kdg.operational.exceptions.RepositoryOperationException;
import be.kdg.operational.repository.PaymentRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import be.kdg.operational.services.paymentHandlers.PaymentHandlerDecider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PaymentService implements ServicesInterface<Payment> {
    private final PaymentRepository repo;
    private CustomerService customerService;
    private PaymentHandlerDecider paymentHandlerDecider;

    public PaymentService(PaymentRepository paymentPersistence, CustomerService customerService, PaymentHandlerDecider paymentHandlerDecider) {
        this.repo = paymentPersistence;
        this.customerService = customerService;
        this.paymentHandlerDecider = paymentHandlerDecider;
    }

    public void handlePayment(Payment payment) throws Exception {
        var customer = customerService.readById(payment.getCustomerID());
        this.save(payment);
        paymentHandlerDecider.handle(payment, customer);
    }

    public Payment save(Payment o) throws RepositoryOperationException {
        try {
            log.info("saving Parcel " + o.getParcelID());
            return repo.save(o);
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Payment.save(): [%s]", ex.getMessage()));
        }
    }

    @Override
    public Payment readById(long id) throws RepositoryOperationException {
        try {
            log.info("finding Parcel " + id);
            var found = repo.findById(id);
            if (!found.isPresent())
                throw new NotFoundException(String.format("Couldn't find Payment with ID: [%s]", id));
            return found.get();
        } catch (NotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Payment.readById(): [%s]", ex.getMessage()));
        }
    }

    public List<Payment> readAll() throws RepositoryOperationException {
        try {
            var list = repo.findAll();
            if (list.isEmpty())
                throw new EmptyRepositoryException("No entries in Payment Repository");
            else
                return list;
        } catch (EmptyRepositoryException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Payment.readAll(): [%s]", ex.getMessage()));
        }
    }


    public void delete(Payment o) throws RepositoryOperationException {
        try {
            log.info("trying to delete payment " + o.getId() + "for parcel " + o.getParcelID());
            var found = this.readById(o.getId());
            if(found != null)
                throw new NotFoundException("Could not delete Payment with id: " +o.getId()+ ". This payment does not exist");
            repo.delete(found);
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Payment.delete(): [%s]", ex.getMessage()));
        }
    }


    //? Searches for a payment related to a parcel
    public Payment findPaymentByParcel(long parcelId) throws RepositoryOperationException {
        log.info("finding Payment for Parcel " + parcelId);
        var found = repo.findPaymentByParcelID(parcelId);
        if (found == null)
            throw new NotFoundException("Cannot find Payment with ParcelId: " + parcelId);
        return found;

    }
}




