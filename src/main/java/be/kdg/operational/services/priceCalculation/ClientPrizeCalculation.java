package be.kdg.operational.services.priceCalculation;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.deliveryRequests.Request;
import be.kdg.operational.services.PricesService;
import org.springframework.stereotype.Component;

@Component
public class ClientPrizeCalculation implements PriceCalculation {

    private PricesService pricesService;

    public ClientPrizeCalculation(PricesService pricesService) {
        this.pricesService = pricesService;
    }

    public double calculate(Request req, Customer client) {
        var bp = pricesService.getBasePrice(req.getPriority());
        var wf = pricesService.getWeightFactor();
        return (bp + req.getWeight() * wf) ;

    }
}
