package be.kdg.operational.services.priceCalculation;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.deliveryRequests.Request;

public interface PriceCalculation {
    double calculate(Request req, Customer customer);
}
