package be.kdg.operational.services.priceCalculation;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.deliveryRequests.Request;
import be.kdg.operational.services.CustomerService;
import be.kdg.operational.services.PricesService;
import org.springframework.stereotype.Component;

@Component
public class CompanyPriceCalculation implements PriceCalculation {
    private PricesService pricesService;
    private CustomerService customerService;

    public CompanyPriceCalculation(PricesService pricesService, CustomerService customerService) {
        this.pricesService = pricesService;
        this.customerService = customerService;
    }

    public double calculate(Request req, Customer company) {
        var bp = pricesService.getBasePrice(req.getPriority());
        var wf = pricesService.getWeightFactor();

        return (bp + req.getWeight() * wf) * company.getReduction();

    }
}
