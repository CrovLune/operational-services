package be.kdg.operational.services.priceCalculation;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.deliveryRequests.Request;
import org.springframework.stereotype.Component;

@Component
public class PriceCalculationDecider {
    private CompanyPriceCalculation companyPriceCalculation;
    private ClientPrizeCalculation clientPrizeCalculation;

    public PriceCalculationDecider(CompanyPriceCalculation companyPriceCalculation, ClientPrizeCalculation clientPrizeCalculation) {
        this.companyPriceCalculation = companyPriceCalculation;
        this.clientPrizeCalculation = clientPrizeCalculation;
    }


    public double calculate(Request req, Customer customer) {
        PriceCalculation calculator;

        switch (customer.getType()) {
            case COMPANY:
                calculator = companyPriceCalculation;
                break;
            default:
                calculator = clientPrizeCalculation;
        }
    var price = calculator.calculate(req, customer);
        return Math.round(price *100) / 100.0;
    }
}
