/**package be.kdg.operational.services;

import be.kdg.operational.domain.customers.Client;
import be.kdg.operational.domain.customers.Company;
import be.kdg.operational.domain.customers.CustomerType;
import be.kdg.operational.domain.deliveryRequests.Request;
import be.kdg.operational.domain.deliveryRequests.enums.Priority;
import be.kdg.operational.domain.tours.PostCode;
import be.kdg.operational.domain.tours.Tour;
import be.kdg.operational.domain.tours.TourStatus;
import be.kdg.operational.domain.vehicles.Size;
import be.kdg.operational.domain.vehicles.Vehicle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Service
@Slf4j
public class ScheduledTasksService {
    private final CustomerService customerService;
    private final PricesService pricesService;
    private final VehicleService vehicleService;
    private final PostCodeService postCodeService;
    private final TourService tourService;

    public ScheduledTasksService(CustomerService customerService, PricesService pricesService, VehicleService vehicleService, PostCodeService postCodeService, TourService tourService) {
        this.customerService = customerService;
        this.pricesService = pricesService;
        this.vehicleService = vehicleService;
        this.postCodeService = postCodeService;
        this.tourService = tourService;
    }

    //? Seed Database with random Companies
    @Scheduled(fixedDelay = (86400000))
    private void createCompanies() {
        var random = new Random();
        for (int i = 0; i < 10; i++) {
            var company = new Company();
            var number = random.nextInt(10000) + 1;
            var reduction = Math.round(random.nextDouble() * 100.0) / 100.0;

            company.setName("Random Company " + number);
            company.setType(CustomerType.COMPANY);
            company.setCredit(-5000);
            company.setCurrentBalance(0);
            company.setReduction(reduction);

            List<Request> list = new ArrayList<>();
            company.setRequests(list);

            customerService.save(company);
        }
        log.info("Companies Created");
    }

    //? Seed Database with random Clients
    @Scheduled(fixedDelay = (86400000))
    private void createClients() {
        var random = new Random();
        for (int i = 0; i < 10; i++) {
            var client = new Client();
            var number = random.nextInt(10000) + 1;

            client.setName("Random Client " + number);
            client.setType(CustomerType.CLIENT);

            List<Request> list = new ArrayList<>();
            client.setRequests(list);

            customerService.save(client);
        }
        log.info("Clients Created");
    }

    //? Seed Database with Basic Price & WeightFactors
    @Scheduled(fixedDelay = (86400000))
    private void createNumbers() {
        var sameDay = new BasicPrice(Priority.SAMEDAY, 15.0);
        var nextDay = new BasicPrice(Priority.NEXTDAY, 8.0);

        pricesService.save(sameDay);
        pricesService.save(nextDay);

        for (int i = 0; i < 25; i++) {
            var weight = new WeightFactor(i + 1, Math.round(((i + 1) * 0.6) * 100.0) / 100.0);
            pricesService.save(weight);
        }
        log.info("Numbers Created");
    }

    //? Seed Database with Vehicles
    @Scheduled(fixedDelay = (86400000))
    public void createVehicles() {
        for (int i = 0; i < 15; i++) {
            Size size;
            var random = new Random();
            var r = random.nextInt(100) + 1;
            if (r * 0.04 < 1) {
                size = Size.SIX;
            } else if (r * 0.04 > 1 && r * 0.04 < 2) {
                size = Size.EIGHT;
            } else if (r * 0.04 > 2 && r * 0.04 < 3) {
                size = Size.ELEVEN;
            } else {
                size = Size.FIFTEEN;
            }
            var vehicle = new Vehicle(null, size);
            vehicleService.save(vehicle);
        }
        log.info("Vehicles Created");
    }

    //? Seed Database with Tours for TODAY
    @Scheduled(fixedDelay = (86400000))
    public void createTodayTours() {
        var time = LocalDateTime.now().plusHours(1);
        var a = 1000;
        for (int i = 0; i < 10; i++) {
            var tour = new Tour();
            tourService.save(tour);
            List<PostCode> postcodes = new ArrayList<>();
            for (int j = 0; j <= 10; j++) {
                var postcode = new PostCode();
                postCodeService.save(postcode);
                postcode.setPostcode(a + j);
                postcode.setTour(tour);
                postCodeService.save(postcode);
                postcodes.add(postcode);
            }
            a += 11;
            var size = 0;
            tour.setPostcodes(postcodes);
            tour.setRouteCapacity(size);
            tour.setPriority(Priority.SAMEDAY);
            tour.setStatus(TourStatus.AT_BASE);
            tour.setStartTime(time);
            tourService.save(tour);
            if (i % 2 == 0) {
                time = time.plusHours(1);
            }
        }
        log.info("Tours Created");
    }

    //? Seed Database with Tours for TOMORROW
    @Scheduled(fixedDelay = (86400000))
    public void createTomorrowTours() {
        var time = LocalDateTime.now().plusDays(1);
        var a = 1000;
        for (int i = 0; i < 10; i++) {
            var tour = new Tour();
            tourService.save(tour);
            List<PostCode> postcodes = new ArrayList<>();
            for (int j = 0; j <= 10; j++) {
                var postcode = new PostCode();
                postCodeService.save(postcode);
                postcode.setPostcode(a + j);
                postcode.setTour(tour);
                postCodeService.save(postcode);
                postcodes.add(postcode);
            }
            a += 11;
            int size = 0;
            tour.setPostcodes(postcodes);
            tour.setRouteCapacity(size);
            tour.setPriority(Priority.NEXTDAY);
            tour.setStatus(TourStatus.AT_BASE);
            tour.setStartTime(time);
            tourService.save(tour);
            if (i % 2 == 0) {
                time = time.plusHours(1);
            }
        }
        log.info("Tours Created");
    }
}
*/