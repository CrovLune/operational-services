package be.kdg.operational.services;

import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.tours.PostCode;
import be.kdg.operational.domain.tours.Tour;
import be.kdg.operational.domain.tours.TourStatus;
import be.kdg.operational.repository.TourRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import javafx.geometry.Pos;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Service
@Transactional
@Slf4j
public class TourService implements ServicesInterface<Tour> {
    private final TourRepository repo;
    private ParcelService parcelService;
    private PostCodeService postCodeService;
    @Value("${minutesBeforeStartShipment}")
    private int minutesBeforeStartShipment;

    public TourService(TourRepository repo, ParcelService parcelService, PostCodeService postCodeService) {
        this.repo = repo;
        this.parcelService = parcelService;
        this.postCodeService = postCodeService;
    }

    @Override
    public Tour save(Tour o) {
        return repo.save(o);
    }

    @Override
    public Tour readById(long id) throws NullPointerException {
        Tour tour = null;
        try {
            if (repo.findById(id).isPresent())
                tour = repo.findById(id).get();
        } catch (NullPointerException exception) {
            log.error(String.format("readById(): Couldn't find TourID: [%s].", id));
        }
        return tour;
    }

    @Override
    public List<Tour> readAll() throws NullPointerException {
        List<Tour> list = null;
        try {
            if (!repo.findAll().isEmpty())
                list = repo.findAll();
        } catch (NullPointerException exception) {
            log.error("readAll(): Couldnt find any Tour.");
        }
        return list;
    }

    @Override
    public void delete(Tour o) {

    }

    public List<Tour> readAllToursOf(LocalDate date) throws NullPointerException {
        List<Tour> list = null;
        try {
            var found = this.readAll()
                    .stream()
                    .filter(t -> t.getStartTime().toLocalDate().equals(date))
                    .collect(Collectors.toList());
            if (!found.isEmpty())
                list = found;
        } catch (NullPointerException exception) {
            log.error(String.format("readAllToursOf(): Couldn't find any Tour with StartTime: [%s]", date));
        }
        return list;
    }

    public List<Tour> getTourByPostcode(PostCode postCode) throws NullPointerException {
        List<Tour> tour = null;
        try {
            var found = this.readAll()
                    .stream()
                    .filter(x -> x.getPostcodes().contains(postCode))
                    .collect(Collectors.toList());
            if (!found.isEmpty())
                tour = found;
        } catch (NullPointerException exception) {
            log.error(String.format("Couldn't find Tour with PostCode: [%s]", postCode.getPostcode()));
        }
        return tour;
    }

    public List<Tour> getReadyForShipmentTours(LocalDate date) throws NullPointerException {
        var now = LocalDateTime.now();
        List<Tour> list = null;
        try {
            //? Get all tours of Today Route
            var found = this.readAll()
                    .stream()
                    .filter(t -> t.getStartTime().toLocalDate().equals(date))
                    .collect(Collectors.toList());
            //? If tour is already on delivery => REMOVE
            found.removeIf(x -> x.getStatus() == TourStatus.ON_DELIVERY);
            //? Remove tours that have not reached max capacity & startTime is not reached yet.
            found.removeIf(
                    x -> x.getRouteCapacity() != x.getMAXCAPACITY().getNumeric()
                            && !x.getStartTime().minusMinutes(minutesBeforeStartShipment).isBefore(now)
            );
            if (!found.isEmpty())
                list = found;
        } catch (NullPointerException exception) {
            log.error(String.format("readAllToursOf(): Couldn't find any Tour with StartTime: [%s]", date));
        }
        return list;
    }


    public void assign(Parcel parcel) throws Exception {
        try {
            //? Find PostCode
            var postcodeInt = parcelService.getPostalCodeFromParcel(parcel);
            var postcode = postCodeService.getPostcodeByInt(postcodeInt);

            //? Find Tour associated with PostCode
            var tours = this.getTourByPostcode(postcode);
            var parcelPriority = parcel.getDeliveryrequest().getPriority();
            Tour tour = null;
            try {
                //TODO put this in own method
                tour = tours.stream().filter(x -> x.getPriority() == parcelPriority).collect(Collectors.toList()).get(0);
            } catch (Exception exception) {
                throw new Exception("problems finding Tour with right priotity");
            }


            //? Check if Tour reached it's capacity
            var isCapacityReached = tour.getRouteCapacity() == tour.getMAXCAPACITY().getNumeric();
            var isOverkill = tour.getRouteCapacity() + parcel.getDeliveryrequest().getSize() > tour.getMAXCAPACITY().getNumeric();
            var isTourBase = tour.getStatus() == TourStatus.AT_BASE;
            //if parcel fits in tour
            if (!isCapacityReached && !isOverkill && isTourBase) {
                var tourParcels = tour.getParcels();
                tourParcels.add(parcel);
                parcel.setTour(tour);
                tour.setRouteCapacity(tour.getRouteCapacity() + parcel.getDeliveryrequest().getSize());

                this.save(tour);
                parcelService.save(parcel);
            } else { //if not, try layer
                parcelService.setParcelStatusDelayed(parcel);
            }
        } catch (Exception ex) {
            throw new Exception("tourAssign: " + ex.getMessage());
        }
    }

}