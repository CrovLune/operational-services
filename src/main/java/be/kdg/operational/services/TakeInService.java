package be.kdg.operational.services;


import be.kdg.operational.controllers.dtos.ParcelTakeInDTO;
import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.services.takeInCheck.TakeInCheck;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class TakeInService {
    private ParcelService parcelService;
    private TourService tourService;
    private List<TakeInCheck> parcelChecks;

    public TakeInService(TourService tourService, ParcelService parcelService, List<TakeInCheck> parcelChecks) {
        this.parcelService = parcelService;
        this.parcelChecks = parcelChecks;
        this.tourService = tourService;
    }

    public void handleTakeIn(ParcelTakeInDTO dto) throws Exception {
        Parcel parcel = null;
        try {
            //? Find Virtual-Parcel
            parcel = parcelService.readById(dto.getParcelId());

            //? Set Parcel arrival time.
            parcelService.setParcelArrivalTime(parcel, dto);

            //perform all checks
            for (var check : parcelChecks) {
                var response = check.check(parcel);
                log.info(check.getClass().getSimpleName() + " on takeIn for Parcel " + parcel.getId()+ ": " + response.getMessage());
                if (!response.isPassed()) {
                    throw new Exception(response.getMessage());
                }
            }

            parcelService.setParcelIsPaid(parcel);

            //TODO fix:
            //tourService.assign(parcel);

        } catch (Exception e) {
            if (parcel != null)
                parcelService.setParcelStatusDelayed(parcel);
            throw new Exception("handleTakeIn: "+ e.getMessage());
        }
    }
}


