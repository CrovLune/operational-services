package be.kdg.operational.services;

import be.kdg.operational.domain.deliveryRequests.enums.Priority;
import be.kdg.operational.domain.prices.PriceData;
import be.kdg.operational.repository.PriceDataRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
@Service
@Transactional
@Slf4j
public class PricesService implements ServicesInterface<PriceData> {
    private final PriceDataRepository priceRepo;

    public PricesService(PriceDataRepository basicPriceRepository) {
        this.priceRepo = basicPriceRepository;
    }


    public double getWeightFactor() {
        return this.readFirst().getWeightfactor();
    }

    public double getBasePrice(Priority priority) {
        double basePrice;
        switch (priority) {
            case NEXTDAY:
                basePrice = this.readFirst().getNextdayprice();
                break;
            default:
                basePrice = this.readFirst().getSamedayprice();
        }
        return basePrice;
    }


    public PriceData save(PriceData o) {
        return priceRepo.save(o);
    }


    public PriceData readById(long id) {
        throw new NotYetImplementedException();

    }

    public PriceData readFirst() {
            return this.readAll().get(0);
    }

    public List<PriceData> readAll() {
        return priceRepo.findAll();
    }

    public void delete(PriceData o) {
        priceRepo.delete(o);
    }


}
