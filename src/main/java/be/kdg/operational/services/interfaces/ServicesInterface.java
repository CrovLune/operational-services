package be.kdg.operational.services.interfaces;

import be.kdg.operational.exceptions.RepositoryOperationException;

import java.util.List;

/**
 * Aleksey Zelenskiy
 * 11/4/2020.
 */
public interface ServicesInterface<T> {
    T save(T o)  throws RepositoryOperationException;
    T readById(long id) throws RepositoryOperationException;
    List<T> readAll()  throws RepositoryOperationException;
    void delete(T o) throws RepositoryOperationException ;
}
