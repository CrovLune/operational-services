package be.kdg.operational.services;

import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.deliveryRequests.Request;
import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.tours.Tour;
import be.kdg.operational.domain.vehicles.Vehicle;
import be.kdg.operational.exceptions.EmptyRepositoryException;
import be.kdg.operational.exceptions.NotFoundException;
import be.kdg.operational.exceptions.RepositoryOperationException;
import be.kdg.operational.repository.VehicleRepository;
import be.kdg.operational.services.interfaces.ServicesInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Service
@Transactional
@Slf4j
public class VehicleService implements ServicesInterface<Vehicle> {
    private final Comparator<Vehicle> minComparator = (o1, o2) -> {
        var a = o1.getCapacity().getNumeric() < o2.getCapacity().getNumeric();
        if (a)
            return 1;
        else
            return 0;
    };
    private final VehicleRepository repo;

    public VehicleService(VehicleRepository repo) {
        this.repo = repo;
    }


    @Override
    public Vehicle save(Vehicle o) {
        try {
            return repo.save(o);
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Vehicle.save(): [%s]", ex.getMessage()));
        }
    }

    @Override
    public Vehicle readById(long id) throws RepositoryOperationException {
        try {
            var found = repo.findById(id);
            if (!found.isPresent())
                throw new NotFoundException(String.format("Couldn't find Vehicle with ID: [%s]", id));
            return found.get();
        } catch (NotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Request.readById(): [%s]", ex.getMessage()));
        }
    }


    @Override
    public List<Vehicle> readAll() throws RepositoryOperationException {
        try {
            var list = repo.findAll();
            if (list.isEmpty())
                throw new EmptyRepositoryException("No entries in Vehicle Repository");
            else
                return list;
        } catch (EmptyRepositoryException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Vehicule.readAll(): [%s]", ex.getMessage()));
        }
    }


    public void delete(Vehicle o) throws RepositoryOperationException {
        try {
            var found = this.readById(o.getId());
            repo.delete(found);
        } catch (Exception ex) {
            throw new RepositoryOperationException(String.format("Vehicule.delete(): [%s]", ex.getMessage()));
        }
    }

    //TODO better errorhandling
    public Vehicle getVehicleForTour(Tour tour) throws NullPointerException {
        Vehicle vehicle = null;
        try {
            var found = this.readAll()
                    .stream()
                    .filter(x -> x.getCapacity().getNumeric() >= tour.getRouteCapacity())
                    .collect(Collectors.toList());
            found.removeIf(x -> x.getTour() != null);
            found = found.stream().min(minComparator).stream().collect(Collectors.toList());
            System.out.println("----------------------------------------------------------------------");
            log.warn("Found car with size: " + found.get(0).getCapacity().getNumeric());
            System.out.println("----------------------------------------------------------------------");
            if (!found.isEmpty())
                vehicle = found.get(0);
        } catch (NullPointerException exception) {
            log.error("getVehicleForTour(): Couldn't find any vehicle for this tour.");
        }
        return vehicle;
    }

}
