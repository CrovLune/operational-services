package be.kdg.operational.repository;

import be.kdg.operational.domain.vehicles.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Repository
public interface ShimpentRepository extends JpaRepository<Vehicle, Long> {
}
