package be.kdg.operational.repository;

import be.kdg.operational.domain.tours.Tour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Repository
public interface TourRepository extends JpaRepository<Tour, Long> {
}
