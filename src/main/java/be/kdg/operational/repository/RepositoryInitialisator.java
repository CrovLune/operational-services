package be.kdg.operational.repository;

import be.kdg.operational.domain.deliveryRequests.enums.Priority;
import be.kdg.operational.domain.tours.PostCode;
import be.kdg.operational.domain.tours.Tour;
import be.kdg.operational.domain.tours.TourStatus;
import be.kdg.operational.services.PostCodeService;
import be.kdg.operational.services.TourService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class RepositoryInitialisator {

    private final TourService tourService;
    private final PostCodeService postCodeService;

    public RepositoryInitialisator(TourService tourService, PostCodeService postCodeService) {
        this.tourService = tourService;
        this.postCodeService = postCodeService;
        createToursAndPostCodes();
    }


    public void createToursAndPostCodes() {
        var time = LocalDateTime.now().plusMinutes(18);
        var a = 1000;
        for (int i = 0; i < 10; i++) {
            var tour = new Tour();
            tourService.save(tour);
            List<PostCode> postcodes = new ArrayList<>();
            for (int j = 0; j <= 10; j++) {
                var postcode = new PostCode();
                postCodeService.save(postcode);
                postcode.setPostcode(a + j);
                postcode.setTour(tour);
                postCodeService.save(postcode);
                postcodes.add(postcode);
            }
            a += 11;
            var size = 0;
            tour.setPostcodes(postcodes);
            tour.setRouteCapacity(size);
            tour.setPriority(Priority.SAMEDAY);
            tour.setStatus(TourStatus.AT_BASE);
            tour.setStartTime(time);
            tourService.save(tour);
        }
    }

}
