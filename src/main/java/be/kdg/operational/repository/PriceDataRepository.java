package be.kdg.operational.repository;

import be.kdg.operational.domain.prices.PriceData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
@Repository
public interface PriceDataRepository extends JpaRepository<PriceData, Long> {
}
