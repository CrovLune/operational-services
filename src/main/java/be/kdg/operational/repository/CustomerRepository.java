
package be.kdg.operational.repository;

import be.kdg.operational.domain.customers.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
