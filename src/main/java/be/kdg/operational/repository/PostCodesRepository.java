package be.kdg.operational.repository;

import be.kdg.operational.domain.tours.PostCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aleksey Zelenskiy
 * 11/6/2020.
 */
@Repository
public interface PostCodesRepository extends JpaRepository<PostCode, Long> {
}
