
package be.kdg.operational.repository;

import be.kdg.operational.domain.deliveryRequests.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryRequestRepository extends JpaRepository<Request, Long> {
}

