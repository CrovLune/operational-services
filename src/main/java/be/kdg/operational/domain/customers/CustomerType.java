package be.kdg.operational.domain.customers;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
public enum CustomerType {

    CLIENT(0),
    COMPANY(1);


    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    CustomerType(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static CustomerType fromString(String t) {
        for (CustomerType b : CustomerType.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }
}
