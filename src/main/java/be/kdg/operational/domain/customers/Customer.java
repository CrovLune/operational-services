package be.kdg.operational.domain.customers;

import be.kdg.operational.domain.deliveryRequests.Request;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
@Entity
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private CustomerType type;
    @Getter
    @Setter
    private double reduction;
    @Getter
    @Setter
    private double credit;
    @Getter
    @Setter
    private double currentbalance;

    //?---------------------------------- Links to other tables ----------------------------------
    @Getter
    @Setter
    @OneToMany(mappedBy = "customer",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Request> requests;

    public Customer(String name, CustomerType type) {
        this.name = name;
        this.type = type;
        this.requests = new ArrayList<>();
    }

    public Customer(long id, String name, CustomerType type) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.requests = new ArrayList<>();
    }

    public Customer() {
    }

    public Customer(String name, CustomerType type, double reduction, double credit, double currentBalance) {
        this.name = name;
        this.type = type;
        this.reduction = reduction;
        this.credit = credit;
        this.currentbalance = currentBalance;
        this.requests = new ArrayList<>();
    }

}


