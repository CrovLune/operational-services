package be.kdg.operational.domain.tours;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "postcodes")
public class PostCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;
    @Getter
    @Setter
    private int postcode;

    //?---------------------------------- Links to other tables ----------------------------------

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Tour tour;

    public PostCode() {
    }

    public PostCode(int postcode) {
        this.postcode = postcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostCode postCode = (PostCode) o;
        return postcode == postCode.postcode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(postcode);
    }

    @Override
    public String toString() {
        return "PostCode{" +
                "id=" + id +
                ", postcode=" + postcode +
                '}';
    }
}
