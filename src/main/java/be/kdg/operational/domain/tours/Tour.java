package be.kdg.operational.domain.tours;

import be.kdg.operational.domain.deliveryRequests.enums.Priority;
import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.vehicles.Size;
import be.kdg.operational.domain.vehicles.Vehicle;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "tours")
public class Tour {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;
    @Setter
    @Getter
    private int routeCapacity;
    @Setter
    @Getter
    private LocalDateTime startTime;
    @Getter
    @Setter
    private Priority priority;
    @Getter
    private final Size MAXCAPACITY = Size.FIFTEEN;
    @Getter
    @Setter
    private TourStatus status;

    //?---------------------------------- Links to other tables ----------------------------------
    @Getter
    @Setter
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = true)
    private Vehicle vehicle;
    @Getter
    @Setter
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PostCode> postcodes;

    @Getter
    @Setter
    @OneToMany(mappedBy = "tour", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Parcel> parcels;


    public Tour() {
    }

    public Tour(Vehicle vehicle, List<PostCode> postcodes, int routeCapacity, LocalDateTime startTime, Priority priority) {
        this.vehicle = vehicle;
        this.postcodes = postcodes;
        this.routeCapacity = routeCapacity;
        this.startTime = startTime;
        this.priority = priority;
        this.status = TourStatus.AT_BASE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tour tour = (Tour) o;
        return id == tour.id &&
                routeCapacity == tour.routeCapacity &&
                Objects.equals(startTime, tour.startTime) &&
                priority == tour.priority &&
                MAXCAPACITY == tour.MAXCAPACITY &&
                Objects.equals(vehicle, tour.vehicle) &&
                Objects.equals(postcodes, tour.postcodes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, routeCapacity, startTime, priority, MAXCAPACITY, vehicle, postcodes);
    }
}
