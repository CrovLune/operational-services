package be.kdg.operational.domain.tours;

import be.kdg.operational.domain.parcels.enums.ParcelDeliveryStatus;

/**
 * Aleksey Zelenskiy
 * 11/7/2020.
 */
public enum TourStatus {
    AT_BASE(0),
    ON_DELIVERY(1);

    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    TourStatus(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static TourStatus fromString(String t) {
        for (TourStatus b : TourStatus.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }
}
