package be.kdg.operational.domain.deliveryRequests.enums;

public enum Priority {
    SAMEDAY(0),
    NEXTDAY(1);

    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    Priority(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static Priority fromString(String t) {
        for (Priority b : Priority.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }

}
