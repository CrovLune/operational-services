package be.kdg.operational.domain.deliveryRequests.enums;

public enum DeliveryRequestState {
    NOT_PAID(0),
    PAID(1),
    MANUAL_ACTION(2);


    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    DeliveryRequestState(int num) {
        this.setNumeric(num);
    }
    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static DeliveryRequestState fromString(String t) {
        for (DeliveryRequestState b : DeliveryRequestState.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }
}
