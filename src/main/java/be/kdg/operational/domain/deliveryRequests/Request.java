package be.kdg.operational.domain.deliveryRequests;


import be.kdg.operational.domain.customers.Customer;
import be.kdg.operational.domain.deliveryRequests.enums.DeliveryRequestState;
import be.kdg.operational.domain.deliveryRequests.enums.NotHomeAction;
import be.kdg.operational.domain.deliveryRequests.enums.Priority;
import be.kdg.operational.domain.parcels.Parcel;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "deliveries")
public class Request implements Serializable {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Getter
    @Setter
    private int weight;
    @Getter
    @Setter
    private int size;
    @Getter
    @Setter
    private Priority priority;
    @Getter
    @Setter
    private String address;
    @Getter
    @Setter
    private NotHomeAction action;
    @Getter
    @Setter
    private boolean parcelArrivedAtWH;

    //?---------------------------------- Links to other tables ----------------------------------
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Getter
    @Setter
    private Parcel parcel;

    @Getter
    @Setter
    //TODO: DELETE
    private DeliveryRequestState requestState;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id", nullable = true)
    private Customer customer;

    public Request(int weight, int size, Priority priority, String address, NotHomeAction action, Parcel parcel, DeliveryRequestState requestState, Customer customer) {
        this.weight = weight;
        this.size = size;
        this.priority = priority;
        this.address = address;
        this.action = action;
        this.parcel = parcel;
        this.requestState = requestState;
        this.customer = customer;
        this.parcelArrivedAtWH = false;
    }

    public Request() {
    }

    @Override
    public String toString() {
        return String.format("customerID: %s | weight: %d | size: %d | priority: %d | adress: %s | action: %d | request State: %d\n"
                , this.getCustomer().getId()
                , this.getWeight()
                , this.getSize()
                , this.getPriority().getNumeric()
                , this.getAddress()
                , this.getAction().getNumeric()
                , this.getRequestState().getNumeric());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return id == request.id &&
                weight == request.weight &&
                size == request.size &&
                priority == request.priority &&
                Objects.equals(address, request.address) &&
                action == request.action &&
                Objects.equals(parcel, request.parcel) &&
                requestState == request.requestState &&
                Objects.equals(customer, request.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, weight, size, priority, address, action, parcel, requestState, customer);
    }
}