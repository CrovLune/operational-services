package be.kdg.operational.domain.prices;

import be.kdg.operational.domain.deliveryRequests.enums.Priority;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
@Entity
@Table(name = "pricedata")
public class PriceData {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Getter
    @Setter
    private double samedayprice;
    @Getter
    @Setter
    private double nextdayprice;
    @Getter
    @Setter
    private double weightfactor;


    public PriceData() {
    }
}
