package be.kdg.operational.domain.payments;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "payments")
public class Payment {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    private  Long customerID;
    @Getter
    @Setter
    private  Long parcelID;
    @Getter
    @Setter
    private  double amount;
    @Getter
    @Setter
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private  LocalDateTime timestamp;

    public Payment(Long customerID, Long parcelID, double amount) {
        this.customerID = customerID;
        this.parcelID = parcelID;
        this.amount = amount;
        this.timestamp = LocalDateTime.now();
    }
    public Payment(Long customerID, double amount) {
        this.customerID = customerID;
        this.amount = amount;
        this.timestamp = LocalDateTime.now();
    }

    public Payment() {
    }
}
