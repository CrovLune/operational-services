package be.kdg.operational.domain.parcels.enums;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
public enum ParcelDeliveryStatus {
    NOT_DELIVERED(0),
    BEING_DELIVERED(1),
    DELIVERED(2);

    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    ParcelDeliveryStatus(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static ParcelDeliveryStatus fromString(String t) {
        for (ParcelDeliveryStatus b : ParcelDeliveryStatus.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }
}
