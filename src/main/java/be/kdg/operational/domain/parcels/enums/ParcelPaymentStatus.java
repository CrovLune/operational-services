package be.kdg.operational.domain.parcels.enums;

/**
 * Aleksey Zelenskiy
 * 11/6/2020.
 */
public enum ParcelPaymentStatus {
    PAID(0),
    NOT_PAID(1),
    MANUAL_ACTION(2);

    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    ParcelPaymentStatus(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static ParcelPaymentStatus fromString(String t) {
        for (ParcelPaymentStatus b : ParcelPaymentStatus.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }
}
