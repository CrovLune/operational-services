package be.kdg.operational.domain.parcels;

import be.kdg.operational.domain.deliveryRequests.Request;
import be.kdg.operational.domain.parcels.enums.ParcelDeliveryStatus;
import be.kdg.operational.domain.parcels.enums.ParcelPaymentStatus;
import be.kdg.operational.domain.tours.Tour;
import be.kdg.operational.domain.vehicles.Vehicle;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "parcels")
public class Parcel implements Serializable {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Getter
    @Setter
    private double price;
    @Getter
    @Setter
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate validUntil;
    @Getter
    @Setter
    private ParcelDeliveryStatus deliverystatus;
    @Getter
    @Setter
    private ParcelPaymentStatus paymentstatus;
    @Getter
    @Setter
    private boolean isdelayed;
    @Getter
    @Setter
    private LocalDateTime receivedatwhtime;


    //?---------------------------------- Links to other tables ----------------------------------
//    @Setter
//    @Getter
//    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JsonIgnore
//    private Response response;

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn
    @JsonIgnore
    private Request deliveryrequest;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "vehicle_id", nullable = true)
    @JsonIgnore
    private Vehicle vehicle;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "tour_id", nullable = true)
    @JsonIgnore
    private Tour tour;

    public Parcel() {
    }

    public Parcel(
            double price,
            LocalDate validUntil,
            ParcelDeliveryStatus deliveryStatus,
            ParcelPaymentStatus paymentStatus
    ) {
        this.price = price;
        this.validUntil = validUntil;
        this.deliverystatus = deliveryStatus;
        this.paymentstatus = paymentStatus;
        this.isdelayed = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parcel parcel = (Parcel) o;
        return Double.compare(parcel.price, price) == 0 &&
                Objects.equals(validUntil, parcel.validUntil) &&
                Objects.equals(deliverystatus, parcel.deliverystatus) &&
                Objects.equals(paymentstatus, parcel.paymentstatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, validUntil, deliverystatus, paymentstatus);
    }

    @Override
    public String toString() {
        return String.format("ID: %s | price: %d | validUntil: %d | deliveryStatus: %d | paymentStatus: %s | requestID: %s | vehicleID: %s\n"
                , this.getId()
                , this.getPrice()
                , this.getValidUntil()
                , this.getDeliverystatus()
                , this.getPaymentstatus()
                , this.getDeliveryrequest().getId()
                , this.getVehicle().getId());
    }
}

