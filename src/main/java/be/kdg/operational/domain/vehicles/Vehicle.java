package be.kdg.operational.domain.vehicles;

import be.kdg.operational.domain.locations.Location;
import be.kdg.operational.domain.parcels.Parcel;
import be.kdg.operational.domain.tours.Tour;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "vehicles")
public class Vehicle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;

    @Getter
    @Setter
    private Size capacity;
    //?---------------------------------- Links to other tables ----------------------------------

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Location location;

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Tour tour;



    public Vehicle() {
    }

    public Vehicle(Location location,Size capacity) {
        this.location = location;
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return id == vehicle.id &&
                Objects.equals(location, vehicle.location) &&
                Objects.equals(tour, vehicle.tour) &&
                capacity == vehicle.capacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, location, tour, capacity);
    }
}
