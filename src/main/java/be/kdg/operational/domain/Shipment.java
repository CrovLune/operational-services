package be.kdg.operational.domain;

import be.kdg.operational.domain.vehicles.Vehicle;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.NumberSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
public class Shipment {
    @Getter
    @Setter
    private long tourId;
    @Getter
    @Setter

    private List<Long> ParcelIds;
    @Getter
    @Setter
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime startDateTime;

    public Shipment() {
    }

    public Shipment(long tourId, List<Long> parcelIds, LocalDateTime startDateTime) {
        this.tourId = tourId;
        ParcelIds = parcelIds;
        this.startDateTime = startDateTime;
    }
}
